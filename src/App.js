import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import ErrorPage from './pages/ErrorPage';
import Login from './pages/Login';
import AboutUs from './pages/AboutUs';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Lookbook from './pages/Lookbook';
import Products from './pages/Products';
import ProductList from './pages/ProductList';
import NewProduct from './pages/NewProduct';
import UserOrder from './pages/UserOrder';
import OrderHistory from './pages/OrderHistory';
import UpdateProduct from './pages/UpdateProduct';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  })

  //Function for clearing the localStorage on logout
  const unsetUser = () =>{
    localStorage.clear()
  }

  useEffect(()=> {

      // fetch("https://whispering-atoll-45857.herokuapp.com/api/users/details", {
      //   headers: {
      //     Authorization:`Bearer ${localStorage.getItem("token")}`
      //   }
      // })
      // .then(res => res.json())
      // .then(data => {

      //   if(typeof data._id !== "undefined") {
      //     setUser({
      //       id: data._id,
      //       isAdmin: data.isAdmin
      //     })
      //   } else {
      //     setUser({
      //       id: null,
      //       isAdmin: null
      //     })
      //   }

      // })
  
  }, [])



  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/aboutus" element={<AboutUs />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/products/:id" element={<Products />} />
            <Route path="/lookbook" element={<Lookbook />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/productlist" element={<ProductList />} />
            <Route path="/newproduct" element={<NewProduct />} />
            <Route path="/userorder" element={<UserOrder />} />
            <Route path="/orderhistory" element={<OrderHistory />} />
            <Route path="/products/:id/update" element={<UpdateProduct />} />
            <Route path="*" element={<ErrorPage />} />
          </Routes>
        </Container>     
      </Router>
    </UserProvider>

  );
}

export default App;
