import { Fragment, useEffect, useState, useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import { Carousel, Table } from "react-bootstrap";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductList() {
  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  const { user, setUser } = useContext(UserContext);
  const { id } = useParams();
 

  const [products, setProducts] = useState([]);
  const fetchProducts = () => {
    fetch("https://whispering-atoll-45857.herokuapp.com/api/products/")
      .then((result) => result.json())
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  function disableProduct(productId) {
      fetch(`https://whispering-atoll-45857.herokuapp.com/api/products/${productId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${ user.token }`
        },
        body: JSON.stringify({
          isActive: false
        }),
      }).then((res) => {
        Swal.fire({
          text: "Disabled Product"
        })
      })

      fetchProducts();
    }


  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      <div id="buttonP" style={{ width: "350px", margin: "0 auto" }}>
        <Link to="/NewProduct">
        <Button variant="secondary" type="submit" id="submitBtn" style={{margin: "0 auto" }}>
          Add New Product
        </Button>
        </Link>

        <Link to="/UserOrder">
        <Button variant="secondary" type="submit" id="submitBtn" style={{margin: "0 auto" }}>
          Show User Orders
        </Button>
        </Link>

      </div>
      
      <div style={{ width: "600px", margin: "0 auto" }}>
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Availability</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          {products.map((product) => (
              <tr>
                <td>{product._id}</td>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.isActive ? 'Available' : 'Not Available'}</td>
                <td> <Link to={`/products/${product._id}/update`}>
              <Button variant="secondary" type="submit" id="submitBtn" style={{margin: "0 auto" }}>
                Update
              </Button>
              </Link> 
                <Button variant="danger" onClick={() => {disableProduct(product._id)}} type="submit" id="submitBtn" style={{margin: "0 auto" }}>
                  Disable
                </Button>
              </td>

              </tr>
            ))}
            
          </tbody>
        </Table>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
      </div>
    </div>
  );
}
