import { Fragment, useState, useEffect, useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
import UserContext from "../UserContext";

export default function Home() {
	const today = new Date();
	const date = today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
	const time =
		today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	const { user, setUser } = useContext(UserContext);
	const [products, setProducts] = useState([]);
	const fetchProducts = () => {
		fetch("https://whispering-atoll-45857.herokuapp.com/api/products/")
			.then((result) => result.json())
			.then((data) => {
				setProducts(data.slice(0, 36));
			});
	};

	useEffect(() => {
		fetchProducts();
	}, []);

	return (
		<div id="logo">
			<div className="text-center">
				<Link to="/Lookbook">
					<img src="images/logo.png" className="img" height="45px" />
				</Link>
				<p>
					{date}&nbsp; {time}&nbsp;PH
				</p>
			</div>

			<div
				className="bg-dark"
				style={{ width: "600px", margin: "0 auto" }}
			>
				{products.map((product) => (
					<Link key={product._id} to={`products/${product._id}`}>
						<img
							src={product.imageUrl}
							style={{
								objectFit: 'none', 
								width: "100px",
								height: "100px",
							}}
						/>
					</Link>
				))}
			</div>

			<div
				style={{
					display: "flex",
					justifyContent: "space-between",
					width: "600px",
					margin: "0 auto",
				}}
			>
				<Link to="/AboutUs" style={{ color: "#808080" }}>
					About Us
				</Link>

				{user.id ? (
					<Fragment>
						<Link to="/Logout" style={{ color: "#808080" }}>
							Logout
						</Link>
						
						{ user.isAdmin ? (
							<Link to="/ProductList" style={{ color: "#808080" }}>
												*Product List
												</Link>
							):(

							<Link to="/OrderHistory" style={{ color: "#808080" }}>
														Order History
													</Link>

							)}
					</Fragment>
				) : (
					<Fragment>
						<Link to="/Login" style={{ color: "#808080" }}>
							Login
						</Link>
						<Link to="/Register" style={{ color: "#808080" }}>
							Register
						</Link>
					</Fragment>	

				)}
					<Link to="/lookbook" style={{ color: "#808080" }}>
					*Featured
					</Link>



			</div>
		</div>
	);
}
