import { Fragment, useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Row, Col, Button, Carousel } from "react-bootstrap";
import UserContext from "../UserContext";

export default function Products() {
  const [product, setProduct] = useState(null);

  const today = new Date();
  const date =
    today.getFullYear() + "/" + (today.getMonth() + 1) + "/" + today.getDate();
  const time =
    today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

  const { id } = useParams();

  const navigate = useNavigate();

  const { user, setUser } = useContext(UserContext);

  const fetchProduct = (id) => {
    fetch(`https://whispering-atoll-45857.herokuapp.com/api/products/${id}`)
      .then((result) => result.json())
      .then((data) => {
        setProduct(data);
      });
  };

  const buyProduct = (product, userId) => {
    fetch(`https://whispering-atoll-45857.herokuapp.com/api/orders/create`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${ user.token }`
      },
      body: JSON.stringify({
        userId: userId,
        product: {
          id: product._id,
          name: product.name,
          description: product.description,
          price: product.price
        },
      })
    })

    navigate('/orderhistory');

  }

  useEffect(() => {
    fetchProduct(id);
  }, []);

  return (
    <div id="logo">
      <div className="text-center">
        <Link to="/">
          <img src="/images/logo.png" className="img" height="45px" />
        </Link>
        <p>
          {date}&nbsp; {time}&nbsp;PH
        </p>
      </div>

      {product && (
        <div>
          <div style={{ width: "350px", margin: "0 auto" }}>
            <Carousel>
              <Carousel.Item interval={1000}>
                <img
                  className="d-block w-100"
                  src={product.imageUrl}
                  height="485px"
                />
                <Carousel.Caption>
                  <h3>{product.name}</h3>
                  <p>{product.description}</p>
                  <p>{product.price}</p>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </div>
        </div>
      )}

      <div style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto"
        }}>

        {user.isAdmin || (
          <Button onClick={() => buyProduct(product, user.id)} variant="danger" type="button" id="submitBtn">
            Buy
          </Button>
          )}

      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          width: "600px",
          margin: "0 auto",
        }}
      >
        <Link to="/" style={{ color: "#808080" }}>
          Home
        </Link>
        <Link to="/AboutUs" style={{ color: "#808080" }}>
          About Us
        </Link>
        {user.id ? (
          <Link to="/Logout" style={{ color: "#808080" }}>
            Logout
          </Link>
        ) : (
          <Link to="/Login" style={{ color: "#808080" }}>
            Login
          </Link>, 
          <Link to="/Register" style={{ color: "#808080" }}>
            Register
          </Link>
        )}
      </div>
    </div>
  );
}
